"""Definitions for MOS 6502/6510-specific code.

This module defines the instruction set, mnemonics (collectively
referred to as 'opcodes'), and other CPU-specific data relating to the
MOS Technologies 6502/6510 series of processors and clones. This
module may be used as a reference for implementing other 8-bit CPU
models.

   :data:`AddressMode`
       An enumeration of the types of addressing modes this CPU's instructions
       may have. The enumeration values should be of type
       :class:`InstrFormat`, which consists of two values: an arbitrary index
       which must be unique within this enumeration, and an instruction size,
       in bytes. The instruction size includes the opcode itself, and any
       operands (e.g. for the 6502, "LDA $0142" is a three-byte
       instruction. One byte for the instruction, and two for the address
       operand).
   :data:`opcodes_mos` 
       The 6502 opcodes for the MOS version of the chip, indexed by decimal
       instruction value.
   :data:`opcodes_mos_mne`
       The 6502 opcodes for the MOS version of the chip, indexed by mnemonic.
   :data:`CPUS`
       A list of CPU data defined in this module.

   :func:`get_pc_instrs`
       Required helper routine which provides all the instructions for this
       CPU which alter the program counter in a non-linear way. Used for
       branch-point identification.
   :func:`make_display_formats`
       Required helper function which produces a mapping of pre-configured
       format strings for opcode output.
"""
from collections import defaultdict, namedtuple
import enum

from . import InstrFormat, Opcode, AddrModeBase, PCModInstrs, OpcodeTable
from .. import __init__ as de64


class AddrMode(InstrFormat, enum.Enum):
    """Addressing mode types for the 6502 instruction set."""
    NONE = AddrModeBase.NONE
    IMPL = InstrFormat(1, 1)
    IMM = InstrFormat(2, 2)
    REL = InstrFormat(3, 2)
    ABS = InstrFormat(4, 3)
    ABS_IDX_X = InstrFormat(5, 3)
    ABS_IDX_Y = InstrFormat(6, 3)
    ZPG = InstrFormat(7, 2)
    ZPG_IDX_X = InstrFormat(8, 2)
    ZPG_IDX_Y = InstrFormat(9, 2)
    IND = InstrFormat(10, 3)
    IND_IDX = InstrFormat(11, 2)
    IDX_IND = InstrFormat(12, 2)
    ACCUM = InstrFormat(13, 1)

    
opcodes_mos = {
    0: Opcode(0, 'BRK', AddrMode.IMPL),
    1: Opcode(1, 'ORA', AddrMode.IDX_IND),
    5: Opcode(5, 'ORA', AddrMode.ZPG),
    6: Opcode(6, 'ASL', AddrMode.ZPG),
    8: Opcode(8, 'PHP', AddrMode.IMPL),
    9: Opcode(9, 'ORA', AddrMode.IMM),
    10: Opcode(10, 'ASL', AddrMode.ACCUM),
    13: Opcode(13, 'ORA', AddrMode.ABS),
    14: Opcode(14, 'ASL', AddrMode.ABS),
    16: Opcode(16, 'BPL', AddrMode.REL),
    17: Opcode(17, 'ORA', AddrMode.IND_IDX),
    21: Opcode(21, 'ORA', AddrMode.ZPG_IDX_X),
    22: Opcode(22, 'ASL', AddrMode.ZPG_IDX_X),
    24: Opcode(24, 'CLC', AddrMode.IMPL),
    25: Opcode(25, 'ORA', AddrMode.ABS_IDX_Y),
    29: Opcode(29, 'ORA', AddrMode.ABS_IDX_X),
    30: Opcode(30, 'ASL', AddrMode.ABS_IDX_X),
    32: Opcode(32, 'JSR', AddrMode.ABS),
    33: Opcode(33, 'AND', AddrMode.IDX_IND),
    36: Opcode(36, 'BIT', AddrMode.ZPG),
    37: Opcode(37, 'AND', AddrMode.ZPG),
    38: Opcode(38, 'ROL', AddrMode.ZPG),
    40: Opcode(40, 'PLP', AddrMode.IMPL),
    41: Opcode(41, 'AND', AddrMode.IMM),
    42: Opcode(42, 'ROL', AddrMode.ACCUM),
    44: Opcode(44, 'BIT', AddrMode.ABS),
    45: Opcode(45, 'AND', AddrMode.ABS),
    46: Opcode(46, 'ROL', AddrMode.ABS),
    48: Opcode(48, 'BMI', AddrMode.REL),
    49: Opcode(49, 'AND', AddrMode.IND_IDX),
    53: Opcode(53, 'AND', AddrMode.ZPG_IDX_X),
    54: Opcode(54, 'ROL', AddrMode.ZPG_IDX_X),
    56: Opcode(56, 'SEC', AddrMode.IMPL),
    57: Opcode(57, 'AND', AddrMode.ABS_IDX_Y),
    61: Opcode(61, 'AND', AddrMode.ABS_IDX_X),
    62: Opcode(62, 'ROL', AddrMode.ABS_IDX_X),
    64: Opcode(64, 'RTI', AddrMode.IMPL),
    65: Opcode(65, 'EOR', AddrMode.IDX_IND),
    69: Opcode(69, 'EOR', AddrMode.ZPG),
    70: Opcode(70, 'LSR', AddrMode.ZPG),
    72: Opcode(72, 'PHA', AddrMode.IMPL),
    73: Opcode(73, 'EOR', AddrMode.IMM),
    74: Opcode(74, 'LSR', AddrMode.ACCUM),
    76: Opcode(76, 'JMP', AddrMode.ABS),
    77: Opcode(77, 'EOR', AddrMode.ABS),
    78: Opcode(78, 'LSR', AddrMode.ABS),
    80: Opcode(80, 'BVC', AddrMode.REL),
    81: Opcode(81, 'EOR', AddrMode.IND_IDX),
    85: Opcode(85, 'EOR', AddrMode.ZPG_IDX_X),
    86: Opcode(86, 'LSR', AddrMode.ZPG_IDX_X),
    88: Opcode(88, 'CLI', AddrMode.IMPL),
    89: Opcode(89, 'EOR', AddrMode.ABS_IDX_Y),
    93: Opcode(93, 'EOR', AddrMode.ABS_IDX_X),
    94: Opcode(94, 'LSR', AddrMode.ABS_IDX_X),
    96: Opcode(96, 'RTS', AddrMode.IMPL),
    97: Opcode(97, 'ADC', AddrMode.IDX_IND),
    101: Opcode(101, 'ADC', AddrMode.ZPG),
    102: Opcode(102, 'ROR', AddrMode.ZPG),
    104: Opcode(104, 'PLA', AddrMode.IMPL),
    105: Opcode(105, 'ADC', AddrMode.IMM),
    106: Opcode(106, 'ROR', AddrMode.ACCUM),
    108: Opcode(108, 'JMP', AddrMode.IND),
    109: Opcode(109, 'ADC', AddrMode.ABS),
    110: Opcode(110, 'ROR', AddrMode.ABS),
    112: Opcode(112, 'BVS', AddrMode.REL),
    113: Opcode(113, 'ADC', AddrMode.IND_IDX),
    117: Opcode(117, 'ADC', AddrMode.ZPG_IDX_X),
    118: Opcode(118, 'ROR', AddrMode.ZPG_IDX_X),
    120: Opcode(120, 'SEI', AddrMode.IMPL),
    121: Opcode(121, 'ADC', AddrMode.ABS_IDX_Y),
    125: Opcode(125, 'ADC', AddrMode.ABS_IDX_X),
    126: Opcode(126, 'ROR', AddrMode.ABS_IDX_X),
    129: Opcode(129, 'STA', AddrMode.IDX_IND),
    132: Opcode(132, 'STY', AddrMode.ZPG),
    133: Opcode(133, 'STA', AddrMode.ZPG),
    134: Opcode(134, 'STX', AddrMode.ZPG),
    136: Opcode(136, 'DEY', AddrMode.IMPL),
    138: Opcode(138, 'TXA', AddrMode.IMPL),
    140: Opcode(140, 'STY', AddrMode.ABS),
    141: Opcode(141, 'STA', AddrMode.ABS),
    142: Opcode(142, 'STX', AddrMode.ABS),
    144: Opcode(144, 'BCC', AddrMode.REL),
    145: Opcode(145, 'STA', AddrMode.IND_IDX),
    148: Opcode(148, 'STY', AddrMode.ZPG_IDX_X),
    149: Opcode(149, 'STA', AddrMode.ZPG_IDX_X),
    150: Opcode(150, 'STX', AddrMode.ZPG_IDX_Y),
    152: Opcode(152, 'TYA', AddrMode.IMPL),
    153: Opcode(153, 'STA', AddrMode.ABS_IDX_Y),
    154: Opcode(154, 'TXS', AddrMode.IMPL),
    157: Opcode(157, 'STA', AddrMode.ABS_IDX_X),
    160: Opcode(160, 'LDY', AddrMode.IMM),
    161: Opcode(161, 'LDA', AddrMode.IDX_IND),
    162: Opcode(162, 'LDX', AddrMode.IMM),
    164: Opcode(164, 'LDY', AddrMode.ZPG),
    165: Opcode(165, 'LDA', AddrMode.ZPG),
    166: Opcode(166, 'LDX', AddrMode.ZPG),
    168: Opcode(168, 'TAY', AddrMode.IMPL),
    169: Opcode(169, 'LDA', AddrMode.IMM),
    170: Opcode(170, 'TAX', AddrMode.IMPL),
    172: Opcode(172, 'LDY', AddrMode.ABS),
    173: Opcode(173, 'LDA', AddrMode.ABS),
    174: Opcode(174, 'LDX', AddrMode.ABS),
    176: Opcode(176, 'BCS', AddrMode.REL),
    177: Opcode(177, 'LDA', AddrMode.IND_IDX),
    180: Opcode(180, 'LDY', AddrMode.ZPG_IDX_X),
    181: Opcode(181, 'LDA', AddrMode.ZPG_IDX_X),
    182: Opcode(182, 'LDX', AddrMode.ZPG_IDX_Y),
    184: Opcode(184, 'CLV', AddrMode.IMPL),
    185: Opcode(185, 'LDA', AddrMode.ABS_IDX_Y),
    186: Opcode(186, 'TSX', AddrMode.IMPL),
    188: Opcode(188, 'LDY', AddrMode.ABS_IDX_X),
    189: Opcode(189, 'LDA', AddrMode.ABS_IDX_X),
    190: Opcode(190, 'LDX', AddrMode.ABS_IDX_Y),
    192: Opcode(192, 'CPY', AddrMode.IMM),
    193: Opcode(193, 'CMP', AddrMode.IDX_IND),
    196: Opcode(196, 'CPY', AddrMode.ZPG),
    197: Opcode(197, 'CMP', AddrMode.ZPG),
    198: Opcode(198, 'DEC', AddrMode.ZPG),
    200: Opcode(200, 'INY', AddrMode.IMPL),
    201: Opcode(201, 'CMP', AddrMode.IMM),
    202: Opcode(202, 'DEX', AddrMode.IMPL),
    204: Opcode(204, 'CPY', AddrMode.ABS),
    205: Opcode(205, 'CMP', AddrMode.ABS),
    206: Opcode(206, 'DEC', AddrMode.ABS),
    208: Opcode(208, 'BNE', AddrMode.REL),
    209: Opcode(209, 'CMP', AddrMode.IND_IDX),
    213: Opcode(213, 'CMP', AddrMode.ZPG_IDX_X),
    214: Opcode(214, 'DEC', AddrMode.ZPG_IDX_X),
    216: Opcode(216, 'CLD', AddrMode.IMPL),
    217: Opcode(217, 'CMP', AddrMode.ABS_IDX_Y),
    221: Opcode(221, 'CMP', AddrMode.ABS_IDX_X),
    222: Opcode(222, 'DEC', AddrMode.ABS_IDX_X),
    224: Opcode(224, 'CPX', AddrMode.IMM),
    225: Opcode(225, 'SBC', AddrMode.IDX_IND),
    228: Opcode(228, 'CPX', AddrMode.ZPG),
    229: Opcode(229, 'SBC', AddrMode.ZPG),
    230: Opcode(230, 'INC', AddrMode.ZPG),
    232: Opcode(232, 'INX', AddrMode.IMPL),
    233: Opcode(233, 'SBC', AddrMode.IMM),
    234: Opcode(234, 'NOP', AddrMode.IMPL),
    236: Opcode(236, 'CPX', AddrMode.ABS),
    237: Opcode(237, 'SBC', AddrMode.ABS),
    238: Opcode(238, 'INC', AddrMode.ABS),
    240: Opcode(240, 'BEQ', AddrMode.REL),
    241: Opcode(241, 'SBC', AddrMode.IND_IDX),
    245: Opcode(245, 'SBC', AddrMode.ZPG_IDX_X),
    246: Opcode(246, 'INC', AddrMode.ZPG_IDX_X),
    248: Opcode(248, 'SED', AddrMode.IMPL),
    249: Opcode(249, 'SBC', AddrMode.ABS_IDX_Y),
    253: Opcode(253, 'SBC', AddrMode.ABS_IDX_X),
    254: Opcode(254, 'INC', AddrMode.ABS_IDX_X),
}

opcodes_mos_mne = { v.mne: v for k, v in opcodes_mos.items() }

def get_pc_instrs():
    """Return a dict of all instructions for this CPU which alter the PC reg.

    :return: Two mappings of all instructions which alter the program
        counter in a non-linear fashion - One for Jumps, JSRs, and any other
        instructions that copy the operand or its target to the program
        counter, and the other for branches that modify the program counter by
        an offset value (BNE, BEQ, &c).  The key for the entries in each
        mapping is the instruction code in decimal as a string (e.g. '42'),
        and the values are :class:`Opcode` instances.
    :rtype: :class:`PCModInstrs`

    This function is required of any CPU module - the instructions
    returned from here are used to identify branch targets and
    routines. This list should not contain RTS-like instructions which
    restore the PC from a previous alteration (e.g. popping the PC from
    the stack).
    """
    instrs = ['BPL', 'BMI', 'BVC', 'BVS', 'BCC', 'BCS', 'BNE', 'BEQ',]
    branch =  { k: opcodes_mos_mne[k] for k in instrs }
    jump = {'JMP': opcodes_mos_mne['JMP'], 'JSR': opcodes_mos_mne['JSR']} 
    rts = {'RTS': opcodes_mos_mne['RTS'],}
    return PCModInstrs(branch, jump, rts)


def make_display_formats(hex_prefix=None, address=False):
    """Return a display format given a configuration namespace.

    :param str hex_prefix: If not None, addresses and operands will be
        formatted as hexadecimal values, prefixed by this value. May be the
        empty string. If None, output is formatted in decimal.
    :param bool address: If true, prefix instructions with their memory address.
    :return: A dict of format strings which accept a :class:`Instruction`
        instance. The key for each format is an
        :class:`de64.opcode.AddrMode`.
    :rtype: dict

    This function is required of any CPU module. Its purpose is to generate
    format-compitable strings which can be used by the disassembler and other
    tools to output an instruction as a string. It  returns a dict of
    format strings referenced by the instruction's addressing mode (see
    :class:`AddrMode`). 

    The formatted output can expect to be passed a single positional argument
    at use time, which is an :class:`~de64.cpu.Opcode` instance.
    """
    mne = '{0.opcode.mne}'
    pfx = hex_prefix
    if pfx:
        addr_fmt = ':04X'
        byte_fmt = f'{pfx}{{0.operand:02X}}'
        sbyte_fmt = f'{pfx}{{0.s_operand:02X}}'
        word_fmt = f'{pfx}{{0.operand:04X}}'
    else:
        addr_fmt = ':05d'
        byte_fmt = '{0.operand:d}'
        sbyte_fmt = '{0.s_operand:d}'
        word_fmt = byte_fmt
    if address:
        addr = f'{{0.address{addr_fmt}}}:\t'
    else:
        addr = ''
    addr_mne = f'{addr}{{0.opcode.mne}}'
    return defaultdict(
        lambda: f'{addr_mne}',
        {
            AddrMode.IMM: f'{addr_mne}\t#{byte_fmt}',
            AddrMode.ABS_IDX_X: f'{addr_mne}\t{word_fmt},X',
            AddrMode.ABS_IDX_Y: f'{addr_mne}\t{word_fmt},Y',
            AddrMode.ZPG_IDX_X: f'{addr_mne}\t{byte_fmt},X',
            AddrMode.ZPG_IDX_Y: f'{addr_mne}\t{byte_fmt},Y',
            AddrMode.IND: f'{addr_mne}\t({word_fmt})',
            AddrMode.IND_IDX: f'{addr_mne}\t({byte_fmt}),Y',
            AddrMode.IDX_IND: f'{addr_mne}\tX,({byte_fmt})',
            AddrMode.ABS: f'{addr_mne}\t{word_fmt}',
            AddrMode.ZPG: f'{addr_mne}\t{byte_fmt}',
            AddrMode.REL: f'{addr_mne}\t{sbyte_fmt}',
        }
    )


class MOS6502:
    """The configuration object for :func:`de64.register_cpu`.

    Every CPU provider module should have at least one configuration
    object. It can be a class, a namedtuple, a data class, or even a dict or
    other mapping - see :func:`de64.register_cpu` for details. The
    configuration object holds references to the data structures and functions
    (or methods) that the tool-set needs to function with a given CPU
    architecture. 
    The following attributes or keys are **required**:

    .. attribute:: name
       :type: str

       The name of this CPU as, e.g., called from the command line. Must be
       unique amongst all CPU instances.

    .. attribute:: instruction_set
       :type: :class:`de64.cpu.OpcodeTable`

       The opcode table for this CPU.

    .. attribute:: pc_instructions
       :type: dict

       A subset of `instruction_set` which contains all instrucitons
       which modify the program counter in a non-linear way.

    .. attribute:: addressing_modes
       :type: enum.Enum

       An enumeration defining the addressing modes of this CPU's instruction
       set. 

    .. attribute:: disasm_formatter
       :type: func

       A function which provides a mapping of addressing types to output
       format strings. Default is :func:`de64.cpu.make_display_formats`

    These attributes or keys are optional. If not present, the CPU defaults
    will be used:

    .. attribute:: cmd_line_config
       :type: func

       A function which, if present and not None, adds cpu-specific options
       to the command-line parser.
    """
    name = 'mos-6502'
    instruction_set = opcodes_mos
    pc_instructions = get_pc_instrs()
    addressing_modes = AddrMode
    disasm_formatter = make_display_formats
    cmd_line_config = None
    

CPUS = [ MOS6502, ]

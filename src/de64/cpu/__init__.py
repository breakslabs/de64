from collections import namedtuple
import enum

InstrFormat = namedtuple('InstrFormat', ['id', 'size'])
InstrFormat.__doc__ = 'Byte size of a given instruction by addressing mode'

Opcode = namedtuple('Opcode', ['val', 'mne', 'addr_mode'])

PCModInstrs = namedtuple('PCModInstrs', ['branch', 'jump', 'rts'])
PCModInstrs.__doc__ = """Instructions which modify the program counter.

:param dict branch: Instructions which branch (PC offset by operand).
:param dict jump: Instructions which jump (set PC to operand).
:param dict rts: Instructions which restore the PC, e.g. from the stack.
"""
PCModInstrs.branch.__doc__ = """
Opcodes which modify the program counter by an offset value (BNE, BEQ, &c.)"""
PCModInstrs.jump.__doc__ = """
Opcodes which change the program counter to a specific value (JMP, JSR, &c)"""
PCModInstrs.rts.__doc__ = """
Opcodes which restore the program counter to a previously stored value"""


class Endian(str, enum.Enum):
    """"Endianness flags.

    .. note::
        Neither the PDPendianness nor the Honeywell 316 endianness are truly
        supported at present, and are here for completeness' sake. Future
        versions may suport them if the demand is present.
    """
    LITTLE = '<'
    BIG = '>'
    PDP = 'PDP'
    HONEYWELL = '316'
    
class AddrModeBase(InstrFormat, enum.Enum):
    """Instruction addressing mode "base" enumeration.

    Enums can't inherit, but all CPU AddrMode Enums should define a
    NONE value as:

        NONE = AddrModeBase.NONE

    Unless they have a reason for non-opcode data to be more than a single 
    byte.
    """
    NONE = InstrFormat(0, 1)


class OpcodeType(enum.Enum):
    """Enum to identify the class a given instruction set belongs to."""
    DOCUMENTED = 'documented'
    UNDOCUMENTED = 'undocumented'
    UNKNOWN = 'unknown'

OpcodeSource = namedtuple('OpcodeSource', ['name', 'opcode_type', 'table'])
OpcodeSource.__doc__ = 'Associate an opcode table with an :class:`OpcodeType` and a name'

class OpcodeTable:
    """Utility class for opcode lookup from multuple sources

    The :attr:`~Opcode.tables` property is a list of :class:`OpcodeSource`
    instances which are searched in list order for the key passed to
    :meth:`__getitem__`. The value returned is from the first table which
    contains that value. If no table contains a value for the passed key,
    :exc:`IndexError` is raised. If the passed key is not an integer value,
    :class:`TypeError` is raised.
    """

    def __init__(self, tables=None):
        """Initialize an OpcodeTable instance.
        
        :param list tables: Optional list of tables in search order. If
            omitted, the default MOS (:data:`opcodes_mos`) table is used.
        """
        if not tables:
            from . import mos6502
            self.tables = [
                OpcodeSource('MOS', OpcodeType.DOCUMENTED, mos6502.opcodes_mos),
                ]
        else:
            self.tables = tables
        
    def __getitem__(self, idx):
        """Return the :class:`Opcode` matching a given instruction value."""
        if not isinstance(idx, int):
            raise TypeError('opcode values must be integers')
        for table in self.tables:
            if idx in table.table:
                return table.table[idx]
        raise IndexError('opcode not found')

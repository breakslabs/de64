"""Code for examining and visualising binary data"""
import codecs
import enum
import itertools
import math
from typing import Iterator
import re
import string

from . import encoding


_hex2_8 = ' '.join(['{:02X}' for i in range(8)])
    
STD_FMT = {
    'canonical': f'{{addr:04X}} {_hex2_8}  {_hex2_8}  |{{string}}|',
    }

_control_chars = bytes([i for i in  itertools.chain(
    [ord('[')],
    range(0,32),
    range(127,160),
    [ord(']')])])

_CONTROL_RE = re.compile(_control_chars)

class NullValueFormatter(string.Formatter):
    """Class to allow replacing missing positional format values.
    
    This allows a one-size-fits-all output format for :func:`datadump` to
    handle the case where there is not enough data left to satisfy the format
    positional argument requirements. It simply replaces the missing fields
    with the same number (as defined by the format specifier) of a given
    string (defaults to space).
    """
    COUNT_RE = re.compile(r'^([0-9]*)(.*)')
    def __init__(self, replacement=' '):
        self._rstring = replacement
        super().__init__()

    def format(self, *args, **kw):
        """Get the positional arg count so we can do our thing."""
        # Not checking for format_string as a kwarg here.
        # It's deprecated and I'm not using it anyway.
        # So drop one for the format string itself.
        self._pos_args = len(args)
        return super().format(*args, **kw)

    def parse(self, format_string):
        """Check if # of positional formats exceed number of arguments."""
        fmts = super().parse(format_string)
        pos_count = 0
        for item in fmts:
            field_name = item[1]
            if item[2]: # Format specifier
                pad = self.COUNT_RE.match(item[2])
                pad = self._rstring * (int(pad.group(1)) if pad else 1)
                if not field_name:
                    pos_count += 1
                try:
                    if (pos_count >= self._pos_args
                            or int(field_name) >= self._pos_args):
                        item = (item[0]+pad, None, None, None)
                except (ValueError, TypeError):
                    pass
            yield item

def replace_control(string, replacement):
    """Replace ASCII/PETSCII control characters in a string with 'replacement'

    :param str string: The string to mogrify.
    :param str replacement: The character to use to replace any control codes
        in the string.
    :return: Mogrified string
    :rtype: str

    This is the default string mogrifier for :func:`datadump`
    """
    return _CONTROL_RE.sub(replacement, string)
         
def datadump(data, fmt='canonical', count=math.inf, addr=0,
             *, bytes_row=16, char_enc='c64-petscii-uc',
                 unprintable='.', mogrify=replace_control):
    """Yield lines of formatted binary data according to a given format.

    :param data: The binary data to format.
    :param str fmt: The output format. May either be a simple string, which
        selects one of the built-in formats (see :data:`STD_FMT`), or a python
        format string. For format string options, see below.
    :param int count: Number of bytes to format. Defaults to math.inf.
    :param int addr: Assumed address for the beginning of the data.
    :param int bytes_row: Number of data bytes to format per output row. Must
        match number of positional fields in format string. Default is 16.
    :param str char_enc: Character encoding to use for character
        output. Default is petscii-c64en-uc (see :mod:`cmbcodecs`).
    :param unprintable: Character to insert in output text to indicate a
        character that is not printable in the given character encoding.
    :param func mogrify: If present, the "printable" string is passeed to this
        function prior to display. The function should accept the string and
        the replacement character (in order), and return a string.
    :type data: bytes or bytearray
    :return: Yields strings
    :rtype: Iterator[str]

    The given format string is passed ``bytes_row`` bytes as positional
    values. In addition, two keyword values are provided. The first, "`addr`",
    is the starting address of the given row of returned data. The second is
    "`string`", and its value is the data row as a string decoded using
    char_enc encoding, with unprintable characters replaced with the
    value of ``unprintable``.
    """
    def dump_replace(exc):
        return (unprintable, exc.end)
    codecs.register_error('dump_replace', dump_replace)
    formatter = NullValueFormatter('-')
    if fmt in STD_FMT:
        fmt = STD_FMT[fmt]
    pos = 0
    mogrify = mogrify if mogrify else (lambda x, y: x)
    mogrify_repl = bytes(unprintable, 'utf-8')
    while pos < count:
        to_read = min(min(bytes_row, len(data[pos:])), count-pos)
        decoded_string = mogrify(data[pos:pos+to_read], mogrify_repl)
        decoded_string = str(decoded_string, char_enc, 'dump_replace')
        decoded_string += ' '*(bytes_row-to_read)
        #print('\n'.join([f'"{i}"' for i in list(decoded_string)]), '\n')
        yield formatter.format(fmt, *data[pos:pos+to_read], addr=addr+pos,
                               string=decoded_string)
        pos += to_read
        if to_read < bytes_row:
            break
        

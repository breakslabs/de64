"""Virtual memory management for de64 tool set

The module provides a virtual memory class and support classes to allow
simulation of a simple machine memory space. The :class:`MemoryManager` class
simulates the address space for a machine, and allows access to individual
memory segments through the standard __getitem__ and __setitem__
interfaces. The memory space is composed of one or more :class:`Memory`
segments, each with an assigned origin and length, and each with its own
access permissions. The memory segments are managed by the
:class:`MemoryManager`, and may overlap in various ways. Each memory segment
is assigned a priority when added to the managed pool. When an action is
performed on a memory address or span of addresses (e.g. a read or a write),
the memory manager first selects all segments which both contain that address,
and have the appropriate access permissions (see :class:`MemoryPerms`) for the
requested operation. If no memory segments exist which match the required
parameters, a :class:`SegmentationFault` is raised. If more than one segment
matches the required parameters, the segment with the highest priority value
is used. If multiple matching segments have the same highest priority,
behavior is undefined - it is up to the application to ensure that two
overlapping memory segments do not have the same priority.

In this fashion, it is possible to emulate, e.g., write-through memory by
inserting two memory segments for the same address range, one with read
permissions and a lower priority, and one with write-only permission and a
higher priority.
"""
from collections import namedtuple
import enum

import intervaltree


Instruction = namedtuple('Instruction', ['opcode', 'operand', 'address'])
Instruction.__doc__ = """Individual instruction encapsulation.

:param opcode: The opcode of this instruction.
:param int operand: The operand for this instruction if any, or None.
:param int address: The memory address this opcode inhabits.
:type opcode: :class:`de64.opcode.Opcode`
"""

class SegmentationFault(Exception):
    pass

class MemoryPerms(enum.Flag):
    NONE = 0
    READ = 1
    WRITE = 2
    RW = READ | WRITE

class MemoryManager:
    """Allows grouping and overlaying :class:`Memory` instances"""
    Segment = namedtuple('Segment', ['org', 'end', 'mem', 'prio'])

    def __init__(self, initial=[]):
        """Initialize a MemoryManager instance.

        :param initial: Any initially populated segments. These will have the
            lowest priority when segment overlap occurs.
        :type initial: list of :class:`Memory`
        """
        self._itree = intervaltree.IntervalTree()
        for m in initial:
            self._itree[m.org:m.end] = self.Segment(m.org, m.end, m, 0)

    def add_segment(self, segment, priority):
        """Add a Memory segment to this managed instance.

        :param segment: The memory segment to add.
        :param int priority: The priority to assign the added memory segment.
        :type segment: :class:`Memory`
        """
        seg = self.Segment(segment.org, segment.end, segment, priority)
        interval = intervaltree.Interval(seg.org, seg.end, seg)
        self._itree.add(interval)


    def __getitem__(self, address):
        """Get a memory value or contiguous array of memory values"""
        if isinstance(address, int):
            return self._get_byte(address)
        elif isinstance(address, slice):
            return self._get_byte_span(address)
        else:
            raise TypeError(f'address "{address}" is not an integer or slice')

    def _get_byte(self, addr):
        """Find the memory segment containing this address - return its value.

        :param int addr: The address to lookup.
        :return: The data value at that address.
        :rtype: int
        :raises SegmentationFault: If no such memory address is mapped.
        """
        segs = sorted(self._itree[addr], key=lambda s: s.data.prio)
        segs = [s.data for s in segs if s.data.mem.perms & MemoryPerms.READ]
        if not segs:
            raise SegmentationFault(f'unmapped memory address "{addr}"')
        return segs.pop().mem[addr]

    def _get_byte_span(self, span):
        """Find the memory segments containing this address span.

        :param int addr: The address span to lookup.
        :return: The data values at that span.
        :rtype: generator
        :raises SegmentationFault: If no such memory address is mapped.
        """
        segs = [s.data for s in self._itree[span.start:span.stop]
                    if s.data.mem.perms & MemoryPerms.READ]
        segs.sort(key=lambda s: s.prio, reverse=True)
        idx = span.start
        cur_seg = None
        while idx < span.stop:
            cur_seg = next((s for s in segs if s.org <= idx <= s.end), None)
            if not cur_seg:
                raise SegmentationFault('incompletely mapped memory span'
                                        f' "({span.start}, {span.stop})"')
            yield cur_seg.mem[idx]
            idx += span.step or 1


class Memory:
    """Represents some contiguous piece of addressable space."""

    def __init__(self, address, data, perms=MemoryPerms.RW):
        """Initialize instance.

        :param int address: The starting address for this memory segment.
        :param data: The data that lives in thie segment.
        :param perms: Memory access permissions.
        :type data: bytes or bytearray
        :type perms: :class:`MemoryPerms`
        """
        self.org = address
        self.data = data
        self.end = self.org + len(data)
        self.perms = perms

    def __getitem__(self, address):
        """Return the byte at 'address'"""
        if isinstance(address, int):
            if not self.org <= address < self.end:
                raise SegmentationFault(f'address out of range: {address}')
            return self.data[address-self.org]
        elif isinstance(address, slice):
            if address.start < self.org or address.stop >= self.end:
                raise SegmentationFault(f'address out of range: '
                                        '{address.start}-{address.stop}')
            translated = slice(address.start-self.org,
                               address.stop-self.org,
                               address.step)
            return self.data[translated]
        else:
            raise TypeError(f'memory address must be an integer or a slice')


    def __len__(self):
        return len(self.data)

from collections import namedtuple
import pathlib

import click

from .platform import c64
from . import disasm
from .cpu import mos6502 as mos6502



def get_cpu(module, cpu_name):
    """Given a CPU python module, fetch a CPU instance.

    :param module: The module which hosts the CPU definition.
    :param str cpu_name: The name of the CPU instance to generate.
    :type module: str or :class:`module`
    :return: A configured CPUDef instance.
    :rtype: :class:`CPUDef`
    :raises KeyError: If cpu_name does not exist.
    """


class AliasedGroup(click.Group):

    def get_command(self, ctx, cmd_name):
        rv = click.Group.get_command(self, ctx, cmd_name)
        if rv is not None:
            return rv
        matches = [x for x in self.list_commands(ctx)
                   if x.startswith(cmd_name)]
        if not matches:
            return None
        elif len(matches) == 1:
            return click.Group.get_command(self, ctx, matches[0])
        ctx.fail('Too many matches: %s' % ', '.join(sorted(matches)))

@click.group(cls=AliasedGroup, chain=True)
@click.pass_context
def cmd_line(ctx):
    ctx.ensure_object(dict)
    ctx.obj['CPU'] = 'mos6502'

@cmd_line.command(name='cpu')
@click.pass_context
@click.argument('name', type=str)
def cpu_cmd(ctx, **kw):
    ctx.obj['CPU'] = kw['name']

@cmd_line.command()
@click.pass_context
@click.argument('filepath', type=pathlib.Path)
@click.option('-o', '--offset', default=0, help='begin disassembly at offset')
@click.option('-org', '--origin', default=0,
              help='define address origin (before offset is applied) '
              '- ignored for PRG files')
@click.option('-x', '--hex', is_flag=True,
              help='output addresses and operands in hexadecimal')
@click.option('-hp', '--hex-prefix', default=None, type=str,
              help='prefix string for hex output of operands - implies --hex')
@click.option('-a', '--addresses', is_flag=True,
              help='output instruction addresses')
@click.option('-P', '--prg-file', is_flag=True,
              help='treat input as a C-64 PRG file')
def disassemble(ctx, **kw):
    if kw.get('hex_prefix', None) or kw.get('hex', None):
        hex_prefix = kw.get('hex_prefix', '0x')
    else:
        hex_prefix = None
    if not kw['prg_file']:
        with kw['filepath'].open('rb') as f:
            data = f.read()
        dis = disasm.Disassembler(data)
        until = len(data)-kw['offset']
    else:
        dis = c64.PrgFile(kw['filepath'])
        until = len(dis)-kw['offset']
    asm = dis.disassemble(kw['offset'], until)
    fmts = mos6502.make_display_formats(hex_prefix, kw['addresses'])
    c_ind = '\n\t\t' if kw['addresses'] else '\n\t'
    for instr in asm:
        print(fmts[instr.opcode.addr_mode].format(instr))

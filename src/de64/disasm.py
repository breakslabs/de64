from collections import namedtuple
import math
import pathlib
import struct

from .cpu import mos6502 
from . import cpu

Instruction = namedtuple('Instruction', ['opcode', 'operand', 's_operand',
                                             'address'])
Instruction.__doc__ = """
Namedtuple containing the relevant values for a single opcode and its operands

:param opcode: The instruction's opcode.
:param int operand: The instruction's operand.
:param int s_operand: The instruction's operand as a signed value.
:param int address: The address of the instruction.
"""



class Disassembler:
    """A basic disassembler class"""

    def __init__(self, mem, *, opcode_table=None, endian = cpu.Endian.LITTLE):
        """Initialize disassembler instance.

        :param mem: The binary :class:`~de64.memory.Memory` to work on.
        :param opcode_table: Optional :class:`~de64.opcode.OpcodeTable` to use
            with this disassembler. If omitted, the default
            :class:`~de64.opcode.OpcodeTable` is used.
        :type mem: bytes or bytearray or :class:`de64.memory.MemoryManager`
        :type opcode_table: :class:`opcodes.OpcodeTable`
        """
        self.opt = opcode_table if opcode_table else cpu.OpcodeTable()
        self.memory = mem
        self.endian = endian
        
    def disassemble(self, address=0, count=math.inf, *, as_instructions=False):
        """Disassemble code at ``address``.

        :param int address: The memory address at which to begin disassembly
        :param int count: Maximum number of bytes/instructions to disassemble.
        :param bool as_instructions: If True, ``count`` indicates the
            number of instructions to return. If False, the minimum number of
            bytes to disassemble (multi-byte instructions may cause the actual
            number of bytes read to be as many as two more than ``count``)
        :return: Disassembled data.
        :rtype: generator of :class:`Instruction`
        :raises de64.memory.SegmentationFault: If address is not
            mapped by the memory manager.
        """
        unpack = [None, 'B', 'H', None, 'I', None, None, None, 'Q']
        pc = address
        opcount = 0
        output = []
        limit = (count, math.inf) if as_instructions else (math.inf, pc+count)
        while opcount < limit[0] and pc < limit[1]:
            try:
                opcode = self.opt[self.memory[pc]]
            except IndexError:
                opcode = cpu.Opcode(self.memory[pc], '---',
                                    cpu.AddrModeBase.NONE)
            obytes = opcode.addr_mode.size - 1
            if obytes > 0:
                operand = struct.unpack(f'{self.endian}{unpack[obytes]}',
                                    bytes(self.memory[pc+1:pc+obytes+1]))[0]
                s_operand = struct.unpack(
                    f'{self.endian}{unpack[obytes].lower()}',
                    bytes(self.memory[pc+1:pc+obytes+1]))[0]
            else:
                operand = None
                s_operand = None
            yield Instruction(opcode, operand, s_operand, pc)
            pc += opcode.addr_mode.size
            opcount += 1
        
        
        
        

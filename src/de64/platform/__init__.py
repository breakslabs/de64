from collections import namedtuple

MemAddr = namedtuple('MemAddr', ['cls', 'start', 'end', 'name', 'descr'])

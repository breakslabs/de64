from de64.disasm import Instruction
from de64.cpu.mos6502 import Opcode, AddrMode

bins = {
    'clockfreq': (b'\xa2C\xa0\x11\x8e\xfa\xff\x8c\xfb\xff\xa9@\x8dA\x11\xa9'
                  b'\x00\x8dB\x11\x8d;\x11\x8d<\x11\x8d=\x11\x8d>\x11\xa9\xfe'
                  b'\x8d?\x11\xa9\xff\x8d@\x11\xadA\x11)\xbf\x8d\x00`\x8dA\x11'
                  b',B\x11\x10\xfb\xa9\x00\x8dB\x11,B\x11\x10\xfb\xa9\x00\x8d'
                  b'B\x11\xad;\x11\x18\xf8iS\x8d;\x11\xad<\x11i\x00\x8d<\x11'
                  b'\xad=\x11i\x00\x8d=\x11\xad>\x11i\x00\x8d>\x11\xd8,B\x11'
                  b'\x10\xd8\xa9\x00\x8dB\x11\xad;\x11\x18\xf8i\x95\x8d;\x11'
                  b'\xad<\x11i\x00\x8d<\x11\xad=\x11i\x00\x8d=\x11\xad>\x11i'
                  b'\x00\x8d>\x11\xd8\xad?\x11\xc9\xc2\xd0\xa9\xad@\x11\xc9'
                  b'\x01\xd0\xa2\xadA\x11\t@\x8d\x00`\x8dA\x11\xa2\x00\xbd'
                  b'\x19\x11\xf0\x07 \x10\x11\xe8L\xb4\x10\xad>\x11HJJJJ\xf0'
                  b'\xf6 \n\x11h)\x0f \n\x11\xa9. \x10\x11\xad=\x11 \xfb\x10'
                  b'\xad<\x11 \xfb\x10\xad;\x11 \xfb\x10\xa2\x00\xbd6\x11\xf0'
                  b'\xdc \x10\x11\xe8L\xec\x10L\x0f\x10HJJJJ \n\x11h)\x0f \n'
                  b'\x11`\t0 \x10\x11`,\x00`P\xfb\x8d\x00P`\rCurrent clock fr'
                  b'equency is \x00 MHz\x00\x00\x00\x00\x00\x00\x00\x00\x00H'
                  b'\xee?\x11\xd0\x03\xee@\x11\xa9\x80\x8dB\x11h@'),
    'crc1': (b'\xa2\x00\xa9\x00\x86\x06\xa0\x08\n&\x06\x90\nI!H\xa5\x06I\x10'
             b'\x85\x06h\x88\xd0\xee\x9d\x00\x80\xa5\x06\x9d\x00\x81\xe8\xd0'
             b'\xdd`E\x07\xaa\xa5\x06]\x00\x81\x85\x07\xbd\x00\x80\x85\x06`'),
    'crc2': (b'\xa2\x00\xa9\x00\x85\x08\x85\x07\x86\x06\xa0\x08Jf\x08f\x07f'
             b'\x06\x90\x16I\xedH\xa5\x08I\xb8\x85\x08\xa5\x07I\x83\x85\x07'
             b'\xa5\x06I \x85\x06h\x88\xd0\xde\x9d\x00\x83\xa5\x08\x9d\x00'
             b'\x82\xa5\x07\x9d\x00\x81\xa5\x06\x9d\x00\x80\xe8\xd0\xbf`E\x06'
             b'\xaa\xa5\x07]\x00\x80\x85\x06\xa5\x08]\x00\x81\x85\x07\xa5\t]'
             b'\x00\x82\x85\x08\xbd\x00\x83\x85\t`'),
    }

prgs = {
    # LDA #$0, STA $ff, LDX #$5, LDA $0080,X
    'dummyprg1': (b'\x00\x00\xa9\x00\x85\xff\xa2\x05\xb5\x80') ,
    # LDA #$19, STA $c0, --- (0x27), LDX #$ca, LDA $0x001,X
    'dummyprg2': (b'\x00\x00\xa9\x19\x85\xc0\x27\xa2\xca\x2d\x00\x20\xb5\x01') ,
}    
    
prg_instructions = {
    'dummyprg1': [
        Instruction(opcode=Opcode(val=169, mne='LDA', addr_mode=AddrMode.IMM),
                    operand=0, s_operand=0, address=0),
        Instruction(opcode=Opcode(val=133, mne='STA', addr_mode=AddrMode.ZPG),
                    operand=255, s_operand = -1, address=2),
        Instruction(opcode=Opcode(val=162, mne='LDX', addr_mode=AddrMode.IMM),
                    operand=5, s_operand=5, address=4),
        Instruction(opcode=Opcode(val=181, mne='LDA', addr_mode=AddrMode.ZPG_IDX_X),
                    operand=128, s_operand=-128, address=6),
        ],
    'dummyprg2': [
        Instruction(opcode=Opcode(val=169, mne='LDA', addr_mode=AddrMode.IMM),
                    operand=0x19, s_operand=0x19, address=0),
        Instruction(opcode=Opcode(val=133, mne='STA', addr_mode=AddrMode.ZPG),
                    operand=192, s_operand=-64, address=2),
        Instruction(opcode=Opcode(val=0x27, mne='---', addr_mode=AddrMode.NONE),
                    operand=None, s_operand=None, address=4),
        Instruction(opcode=Opcode(val=162, mne='LDX', addr_mode=AddrMode.IMM),
                    operand=0xca, s_operand=-0x36, address=5),
        Instruction(opcode=Opcode(val=0x2d, mne='AND', addr_mode=AddrMode.ABS),
                    operand=0x2000, s_operand=0x2000, address=7),
        Instruction(opcode=Opcode(val=181, mne='LDA', addr_mode=AddrMode.ZPG_IDX_X),
                    operand=0x01, s_operand=0x01, address=10),
        ],
    }

alt_opcodes = {
    'set1': {
        0x00: Opcode(0x00, 'BH', AddrMode.IMPL),
        0xa9: Opcode(0xa9, 'CPAR', AddrMode.IMM),
        0x85: Opcode(0x85, 'HCF', AddrMode.ZPG),
        0xa2: Opcode(0xa2, 'CDS', AddrMode.IMM),
        0xb5: Opcode(0xb5, 'BPO', AddrMode.ZPG_IDX_X),
        0x2d: Opcode(0x2d, 'BMR', AddrMode.ABS),
        },
    }

alt_instructions = {
    'dummyprg1': {
        'set1': [
            Instruction(alt_opcodes['set1'][0x00], None, None, 0),
            Instruction(alt_opcodes['set1'][0x00], None, None, 1),
            Instruction(alt_opcodes['set1'][0xa9], 0, 0, 2),
            Instruction(alt_opcodes['set1'][0x85], 255, -1, 4),
            Instruction(alt_opcodes['set1'][0xa2], 5, 5, 6),
            Instruction(alt_opcodes['set1'][0xb5], 128, -128, 8),
            ],
        },
    }

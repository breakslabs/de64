import io

class PseudoPath:
    """Mock-ish of a pathlib.Path object."""
    class Manager:
        def __init__(self, io):
            self.file = io

        def __enter__(self):
            return self.file

        def __exit__(self, *args, **kw):
            pass

        def __getattr__(self, attr):
            if hasattr(self.__dict__['file'], attr):
                return getattr(self.__dict__['file'], attr)
            raise AttributeError('"PseudoPath" object has on attribute '
                                 f'"{attr}"')
        
    def __init__(self, data):
        if isinstance(data, bytes):
            self.file = io.BytesIO(data)
        else:
            self.file = io.StringIO(data)

    def exists(self):
        return True

    def open(self, mode):
        return self.Manager(self.file)

        

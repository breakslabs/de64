import enum

import pytest
parametrize = pytest.mark.parametrize

from de64 import cpu

# General CPU Tests

@pytest.fixture
def test_addr_modes():
        class AddrModes(cpu.InstrFormat, enum.Enum):
            NONE = cpu.AddrModeBase.NONE
            IMM = cpu.InstrFormat(1, 1)
            OTH = cpu.InstrFormat(2, 2)
            THG = cpu.InstrFormat(3, 2)
            BIG = cpu.InstrFormat(4, 4)
        return AddrModes

@pytest.fixture
def test_opcodes(test_addr_modes):
    opcodes_test = {
        0: cpu.Opcode(0, 'CLBR', test_addr_modes.IMM),
        10: cpu.Opcode(10, 'CRN', test_addr_modes.BIG),
        14: cpu.Opcode(14, 'CU', test_addr_modes.OTH),
        18: cpu.Opcode(18, 'DNPG', test_addr_modes.THG),
        54: cpu.Opcode(54, 'EIOC', test_addr_modes.OTH),
        }
    return opcodes_test
    
@parametrize('opcode', [0, 10, 14, 18, 54])
def test_custom_opcode_tables(opcode, test_opcodes):
    ops = cpu.OpcodeSource('TEST', cpu.OpcodeType.DOCUMENTED, test_opcodes)
    ot = cpu.OpcodeTable(tables=[ops])
    assert ot[opcode] == test_opcodes[opcode]

@parametrize('opcode', [None, "test", 43.21, b'\x00'])
def test_opcode_table_type_error(opcode):
    ot = cpu.OpcodeTable()
    with pytest.raises(TypeError):
        assert ot[opcode]
    

        

from collections import namedtuple

import pytest
parametrize = pytest.mark.parametrize

from de64 import disasm
from de64.cpu import mos6502 

from . import data_6502


@parametrize('code,table,expected',
             [(data_6502.prgs['dummyprg1'], data_6502.alt_opcodes['set1'],
               data_6502.alt_instructions['dummyprg1']['set1'])])
def test_disasm_opcode_table(code, table, expected):
    d = disasm.Disassembler(code, opcode_table=table)
    for a, b in zip(expected, d.disassemble()):
                    assert a == b

ArgsTest = namedtuple('TestArgs', ['hex', 'address'])
@parametrize('code,opcodes,args,instr,expected',
             [
    (data_6502.bins['crc1'], None, ArgsTest(None, False), 9, 'LDA\t6'),
    (data_6502.bins['crc1'], None, ArgsTest(None, True), 9, '00016:\tLDA\t6'),
    (data_6502.bins['crc1'], None, ArgsTest('0x', True), 9, '0010:\tLDA\t0x06'),
    (data_6502.bins['crc1'], None, ArgsTest(None, False), 17, 'STA\t33024,X'),
    (data_6502.bins['crc1'], None, ArgsTest('0x', False), 17, 'STA\t0x8100,X'),
    (data_6502.bins['crc1'], None, ArgsTest('^&', False), 17, 'STA\t^&8100,X'),
                   ])
def test_output_formats(code, opcodes, args, instr, expected):
    d = disasm.Disassembler(code, opcode_table=opcodes)
    i = list(d.disassemble(count=len(code)))[instr]
    fmts = mos6502.make_display_formats(args.hex, args.address)
    assert fmts[i.opcode.addr_mode].format(i) == expected

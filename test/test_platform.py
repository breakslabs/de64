import pathlib

import pytest
parametrize = pytest.mark.parametrize

from de64.platform import c64

from . import PseudoPath
from . import data_6502


class PathPatch:
    def __init__(self, pathdict):
        self._pathdict = pathdict

    def __call__(self, filepath):
        return PseudoPath(self._pathdict[filepath])

### C64 platform tests

@parametrize('bin', [data_6502.bins['clockfreq'],
                         data_6502.bins['crc1']])
def test_load_prg(bin):
    p = PseudoPath(bin)
    m = c64.load_prg(p)

    assert isinstance(m, c64.memory.Memory)
    org = bin[0] | (bin[1]<<8)
    assert m[org+5:org+9] == bin[7:11]
    assert len(m) == len(bin)-2


@parametrize('binname', ['clockfreq','crc1',])
def test_load_prg_strpath(monkeypatch, binname):
    monkeypatch.setattr(pathlib, 'Path', PathPatch(data_6502.bins))
    m = c64.load_prg(binname)
    # Undo is required, else pytest explodes when an assertion fails
    # and it attempts to use the now-patched Path class internally
    monkeypatch.undo()
    assert isinstance(m, c64.memory.Memory)
    org = data_6502.bins[binname][0] | (data_6502.bins[binname][1]<<8)
    assert m[org+5:org+9] == data_6502.bins[binname][7:11]
    assert len(m) == len(data_6502.bins[binname])-2

@parametrize('prgname', ['dummyprg1', 'dummyprg2'])
def test_prgfile(prgname):
    import sys
    p = c64.PrgFile(PseudoPath(data_6502.prgs[prgname]))
    for a, b in zip(p.disassemble(0), data_6502.prg_instructions[prgname]):
        assert a == b
    assert len(p) == len(data_6502.prgs[prgname])-2

@parametrize('prgname', ['dummyprg1', 'dummyprg2'])
def test_prgfile_strpath(monkeypatch, prgname):
    monkeypatch.setattr(pathlib, 'Path', PathPatch(data_6502.prgs))
    p = c64.PrgFile(prgname)
    # Undo is required, else pytest explodes when an assertion fails
    # and it attempts to use the now-patched Path class internally
    monkeypatch.undo()
    for a, b in zip(p.disassemble(0), data_6502.prg_instructions[prgname]):
        assert a == b
    assert len(p) == len(data_6502.prgs[prgname])-2

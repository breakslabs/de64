from collections import namedtuple

import pytest
parametrize = pytest.mark.parametrize

from de64 import memory

from .data_6502 import bins


# Memory tests

@parametrize('load_addr,binary,address,value',
        [(0x0800, bins['crc1'], 0x0820, bins['crc1'][0x20]),
         (0xC000, bins['clockfreq'], 0xC0A1, bins['clockfreq'][0xA1]),
         (0x0000, bins['clockfreq'], slice(10,18), bins['clockfreq'][10:18]),
    ])
def test_mem(load_addr, binary, address, value):
    mem = memory.Memory(load_addr, binary)
    assert mem[address] == value

@parametrize('addr',
      [5, slice(0x07f0, 0x0810), 0xc000])
def test_mem_segfault(addr):
    mem = memory.Memory(0x0800, bins['crc2'])
    with pytest.raises(memory.SegmentationFault):
        x = mem[addr]

@parametrize('addr',
      ['not an int', object, lambda x: x, None])
def test_mem_typeerror(addr):
    mem = memory.Memory(0x0800, bins['crc2'])
    with pytest.raises(TypeError):
        x = mem[addr]

## MemoryManager tests

@parametrize('address,value',
      [(0x0801, 0xff), (0x0820, bins['crc1'][0x10]),
       (slice(0x080d, 0x0814), b'\xff\xff\xff'+bins['crc1'][0:4]),
           ])
def test_mem_manager(address, value):
    mem1 = memory.Memory(0x780, b'\xff'*256, memory.MemoryPerms.RW)
    mem2 = memory.Memory(0x810, bins['crc1'], memory.MemoryPerms.RW)
    mman = memory.MemoryManager()
    mman.add_segment(mem1, 0)
    mman.add_segment(mem2, 10)
    if isinstance(address, slice):
        assert bytes(mman[address]) == value
    else:
        assert mman[address] == value

@parametrize('address', [None, test_mem, 'test', b'\x23',])
def test_mem_manager_type_error(address):
    mm = memory.MemoryManager()
    with pytest.raises(TypeError):
        assert mm[address]

@parametrize('address,mapped',
   [(0x200, False), (0x800, True), (slice(0x07a0, 0x0800), False),
        (slice(0x802, 0x810), True),])
def test_mem_manager_segfault(address, mapped):
    mem = memory.Memory(0x0800, bins['crc2'])
    mman = memory.MemoryManager([mem])
    if mapped:
        x = mman[address]
        if isinstance (address, slice):
            x = list(mman[address])
    else:
        with pytest.raises(memory.SegmentationFault):
            x = mman[address]
            if isinstance (address, slice):
                x = list(mman[address])

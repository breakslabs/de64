DE-64
=====

Yet another 6502 disassembler, with a focus on the Commodore 64 and
its kernal and memory structure. At present, does little more than
disassemble 6502 code, but there are plans. Oh boy, are there plans.

Mostly I just want a 6510/C64 disassembler that plays the way I
want it to for reverse-engineering existing machine code. What we used
to do with a pencil and reams of disassembly dumped to tractor-feed -
but more dynamic, and automated where possible.

Just for show-off sake, there is a command line tool of sorts
included. It's just this side of functional at present.



Command Line Usage
------------------

Currently the command line tool does nothing more than linear
disassembly of a binary file with an optional offset. It makes no
effort at memory labeling, data chunk identification, undocumented
opcodes, &c. It may trip over a data chunk, and start spitting out
gibberish. Be nice to it.

The command line tool is called de64, and is installed by default via
setuptools. The syntax is::

  usage: de64 [-h] [-o OFFSET] [-org ORIGIN] [-x [PREFIX]] [-a] [-P] binary

  positional arguments:
    binary                Binary file to disassemble

  optional arguments:
    -h, --help            show this help message and exit
    -o OFFSET, --offset OFFSET
                          Offset within file at which to begin (default: 0)
    -org ORIGIN, --origin ORIGIN
                          Assumed program origin. Ignored for PRG files.
                          (default: 0)
    -x [PREFIX], --hex [PREFIX]
                          Print values in hex instead of decimalwith optional
                          prefix (default: )
    -a, -addresses        Print instruction addresses (default: False)
    -P, --prg-file        Treat binary as a Commodore PRG file (default: False)


Module usage
------------

Basically a big bag of "read the docstrings" at the moment. See the homepage
at http://breakslabs.gitlab.io/de64

Future Plans
------------

Things in the pipe include:

- Automatic address labeling
- CPU variant handling and undocumented opcodes
- Data block identification heuristics
- C-64 Memory map referencing and documenting
- C-64 Kernal subroutine identification and documenting
- Possible 1541 memory mapping, &c.
- Common C-64 trope identification (memory configuring, unpacking,
  &c.)
- ACTIVE ANNOTATION.

Active Annotation
~~~~~~~~~~~~~~~~~

In addition to straight disassembling of a binary blob, future
versions will allow to provide an associated file (maybe JSON or YAML
or XML or something else) that allows the disassembler to annotate the
output with assigned labeling, comments, "function" definitions, and
maybe some other things. Further, the automated disassembly process
can produce such a file, allowing for iterative disassembly and code
review.

.. de64 documentation master file, created by
   sphinx-quickstart on Sun Jun  7 14:04:10 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

DE-64 - A 6502/6510 disassembler suite.
=======================================

DE-64 is a collection of tools used to examine 6502/6510 machine code and
binary data. It is intended to specifically assist in examing 6510 code in
relation to the Commodore 64 personal computer. In the future, it should be
possible to add additional machine specifics (VIC-20, C-128, &c.), and
possibly even additional CPUs (z-80, 8008, 6800, &c.). At present, it provides
a simple disassembler and tools for dumping memory in a ``hexdump(1)`` style
format, complete with PETSCII encoding.

Presently in a mixed state of usefulness.

.. toctree::
   :maxdepth: 3
   :caption: Contents:

   de64
   cpu
   platform
	     



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

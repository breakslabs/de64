CPU
===

This module contains code specific to a given CPU type or instance. It
includes instructions and mnemonic information, addressing mode information,
memory size and paging, &c. At present, only the MOS Technologies 6502
microprocessor is present, but it should be possible to easily add other
(8-bit) CPUs. For an example, see the provided :mod:`~de64.cpu.mos6502`
module.


de64.cpu
--------

.. automodule:: de64.cpu
   :members:
   :undoc-members:
   :show-inheritance:


de64.cpu.mod6502
----------------

.. automodule:: de64.cpu.mos6502
   :members:
   :show-inheritance:
